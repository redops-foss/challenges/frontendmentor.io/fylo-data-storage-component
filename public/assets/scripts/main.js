window.onload = function() {
  const MAX = 1000;
  const CURRENT = 0;
  const TARGET = 815;
  const DURATION = 2000;

  const easeInOutQuad = (a, b, c, d) => {
    let t = a;
    t /= (d / 2);
    if (t < 1) {
      return (c / 2 * t * t) + b;
    }
    t--;
    return -(c / 2 * ((t * (t - 2)) - 1)) + b;
  };

  const animateUsage = (duration = 2000, callback) => {
    const timeIncrement = 20;
    let currentTime = 0;
    const animate = () => {
      currentTime += timeIncrement;
      callback(Math.round(easeInOutQuad(currentTime, CURRENT, TARGET, duration)));
      if (currentTime < duration) {
        setTimeout(animate, timeIncrement);
      }
    };
    animate();
  };

  const currentUsageElement = document.getElementById('current-usage');
  const usageLeftElement = document.getElementById('usage-left');
  const usageBarElement = document.getElementById('usage-bar-content');

  const editDocument = (current) => {
    currentUsageElement.textContent = current;
    usageLeftElement.textContent = `${MAX - current}`;

    usageBarElement.style.width = `${100 * current / MAX}%`;
  }

  animateUsage(DURATION, editDocument);

  window.animate = () => animateUsage(DURATION, editDocument);
}
